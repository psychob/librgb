#include "./../rgb/noise_generator.hpp"

#include "./../rgb/noise_math.hpp"

namespace rgb
{
 noise_integral::noise_integral( uint32 seed )
  : noise_integral_base( seed )
 { }

 float64 noise_integral::get( float64 x )
 {
  return noise_integral_generator( (int32)x, _seed );
 }

 float64 noise_integral::get( float64 x, float64 y )
 {
  return noise_integral_generator( (int32)x, (int32)y, _seed );
 }

 float64 noise_integral::get( float64 x, float64 y, float64 z )
 {
  return noise_integral_generator( (int32)x, (int32)y, (int32)z, _seed );
 }

 float64 noise_integral::get( int32 x )
 {
  return noise_integral_generator( x, _seed );
 }

 float64 noise_integral::get( int32 x, int32 y )
 {
  return noise_integral_generator( x, y, _seed );
 }

 float64 noise_integral::get( int32 x, int32 y, int32 z )
 {
  return noise_integral_generator( x, y, z, _seed );
 }

 float64 noise_integral_generator( int32 x, uint32 seed )
 {
  int32 r = ( x * 13 ) + ( seed * 57 ) + 13;
        r = ( r >> 13 ) ^ r;
        r = ( r * ( r * r * 60493 + 19990303 ) + 1376312589 ) & 0x7fffffff;
  return 1.0 - ( (float64)r / 1073741824.0 );
 }

 float64 noise_integral_generator( int32 x, int32 y, uint32 seed )
 {
  int32 r = ( x * 13 ) + ( y * 57 ) + ( seed * 71 ) + 13;
        r = ( r >> 13 ) ^ r;
        r = ( r * ( r * r * 60493 + 19990303 ) + 1376312589 ) & 0x7fffffff;
  return 1.0 - ( (float64)r / 1073741824.0 );
 }

 float64 noise_integral_generator( int32 x, int32 y, int32 z, uint32 seed )
 {
  int32 r = ( x * 13 ) + ( y * 57 ) + ( z * 71 ) + ( seed * 107 ) + 13;
        r = ( r >> 13 ) ^ r;
        r = ( r * ( r * r * 60493 + 19990303 ) + 1376312589 ) & 0x7fffffff;
  return 1.0 - ( (float64)r / 1073741824.0 );
 }
}

namespace rgb
{
 noise_coherent::noise_coherent( quality q, uint32 seed )
  : noise_coherent_base( q, seed )
 { }

 float64 noise_coherent::get( float64 x )
 {
  return noise_coherent_generator( x, (int32)x, _quality, _seed );
 }

 float64 noise_coherent::get( float64 x, float64 y )
 {
  return noise_coherent_generator( x, y, (int32)x, (int32)y, _quality, _seed );
 }

 float64 noise_coherent::get( float64 x, float64 y, float64 z )
 {
  return noise_coherent_generator( x, y, z, (int32)x, (int32)y, (int32)z,
                                   _quality, _seed );
 }

 float64 noise_coherent_generator( float64 x, noise_coherent_base::quality q,
                                   uint32 seed )
 {
  return noise_coherent_generator( x, (int32)x, q, seed );
 }

 float64 noise_coherent_generator( float64 x, float64 y,
                                   noise_coherent_base::quality q, uint32 seed )
 {
  return noise_coherent_generator( x, y, (int32)x, (int32)y, q, seed );
 }

 float64 noise_coherent_generator( float64 x, float64 y, float64 z,
                                   noise_coherent_base::quality q, uint32 seed )
 {
  return noise_coherent_generator( x, y, z, (int32)x, (int32)y, (int32)z, q,
                                   seed );
 }

 float64 noise_coherent_generator( float64 fx, int32 ix,
                                   noise_coherent_base::quality q, uint32 seed )
 {
  float64 d0, d1;

  d0 = noise_integral_generator( ix, seed );
  d1 = noise_integral_generator( ix + 1, seed );

  float64 t;

  switch ( q )
  {
   case noise_coherent_base::quality::low:
    t = scurve0( fx - (float64)ix );
    break;

   case noise_coherent_base::quality::normal:
    t = scurve3( fx - (float64)ix );
    break;

   case noise_coherent_base::quality::high:
    t = scurve5( fx - (float64)ix );
    break;
  }

  return lerp( d0, d1, t );
 }

 float64 noise_coherent_generator( float64 fx, float64 fy, int32 ix, int32 iy,
                                   noise_coherent_base::quality q, uint32 seed )
 {
  float64 d00, d01, d10, d11;

  d00 = noise_integral_generator( ix, iy, seed );
  d01 = noise_integral_generator( ix + 1, iy, seed );
  d10 = noise_integral_generator( ix, iy + 1, seed );
  d11 = noise_integral_generator( ix + 1, iy + 1, seed );

  float64 tx, ty;

  switch ( q )
  {
   case noise_coherent_base::quality::low:
    tx = scurve0( fx - (float64)ix );
    ty = scurve0( fy - (float64)iy );
    break;

   case noise_coherent_base::quality::normal:
    tx = scurve3( fx - (float64)ix );
    ty = scurve3( fy - (float64)iy );
    break;

   case noise_coherent_base::quality::high:
    tx = scurve5( fx - (float64)ix );
    ty = scurve5( fy - (float64)iy );
    break;
  }

  float64 i1, i2;

  i1 = lerp( d00, d01, tx );
  i2 = lerp( d10, d11, tx );

  return lerp( i1, i2, ty );
 }

 float64 noise_coherent_generator( float64 fx, float64 fy, float64 fz,
                                   int32 ix, int32 iy, int32 iz,
                                   noise_coherent_base::quality q, uint32 seed )
 {
  float64 d000, d001, d010, d011, d100, d101, d110, d111;

  d000 = noise_integral_generator( ix, iy, iz, seed );
  d001 = noise_integral_generator( ix, iy, iz + 1, seed );
  d010 = noise_integral_generator( ix, iy + 1, iz, seed );
  d011 = noise_integral_generator( ix, iy + 1, iz + 1, seed );
  d100 = noise_integral_generator( ix + 1, iy, iz, seed );
  d101 = noise_integral_generator( ix + 1, iy, iz + 1, seed );
  d110 = noise_integral_generator( ix + 1, iy + 1, iz, seed );
  d111 = noise_integral_generator( ix + 1, iy + 1, iz + 1, seed );

  float64 tx, ty, tz;

  switch ( q )
  {
   case noise_coherent_base::quality::low:
    tx = scurve0( fx - ix );
    ty = scurve0( fy - iy );
    tz = scurve0( fz - iz );
    break;

   case noise_coherent_base::quality::normal:
    tx = scurve3( fx - ix );
    ty = scurve3( fy - iy );
    tz = scurve3( fz - iz );
    break;

   case noise_coherent_base::quality::high:
    tx = scurve5( fx - ix );
    ty = scurve5( fy - iy );
    tz = scurve5( fz - iz );
    break;
  }

  float64 i00, i01, i10, i11;

  i00 = lerp( d000, d100, tx );
  i01 = lerp( d010, d110, tx );
  i10 = lerp( d001, d101, tx );
  i11 = lerp( d011, d111, tx );

  float64 c0, c1;

  c0 = lerp( i00, i01, tz );
  c1 = lerp( i10, i11, tz );

  return lerp( c0, c1, ty );
 }
}
