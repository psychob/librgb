#include "./../rgb/allocators_helpers.hpp"

#include "./../rgb/allocators_heap.hpp"

namespace rgb
{
 allocator * allocator_get( )
 {
  static allocator_heap _;
  return &_;
 }
}
