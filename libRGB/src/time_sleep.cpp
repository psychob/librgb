#include "./../rgb/time_sleep.hpp"

#include "./../rgb/internal/winapi-include.hpp"
#include "./../rgb/logger_macros.hpp"

#pragma comment(lib, "Winmm.lib")

namespace rgb
{
 void sleep( const time_span & ts )
 {
  auto milli = ts.get_milli( );

  if ( milli == INFINITE )
   milli--;

  // msdn: https://msdn.microsoft.com/en-us/library/ms686298%28VS.85%29.aspx
  // This function causes a thread to relinquish the remainder of its time
  // slice and become unrunnable for an interval based on the value of
  // dwMilliseconds. The system clock "ticks" at a constant rate. If
  // dwMilliseconds is less than the resolution of the system clock, the thread
  // may sleep for less than the specified length of time. If dwMilliseconds
  // is greater than one tick but less than two, the wait can be anywhere
  // between one and two ticks, and so on. To increase the accuracy of the
  // sleep interval, call the timeGetDevCaps function to determine the
  // supported minimum timer resolution and the timeBeginPeriod function to
  // set the timer resolution to its minimum. Use caution when calling
  // timeBeginPeriod, as frequent calls can significantly affect the system
  // clock, system power usage, and the scheduler. If you call
  // timeBeginPeriod, call it one time early in the application and be sure
  // to call the timeEndPeriod function at the very end of the application.
  TIMECAPS tc = { 0 };
  timeGetDevCaps( &tc, sizeof( TIMECAPS ) );
  timeBeginPeriod( tc.wPeriodMin );
  Sleep( milli );
  timeEndPeriod( tc.wPeriodMin );
 }
}
