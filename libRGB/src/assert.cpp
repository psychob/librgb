#include "./../rgb/assert.hpp"

#include "./../rgb/string_length.hpp"
#include "./../rgb/string_cast_encoding.hpp"
#include "./../rgb/internal/macro-module.hpp"

RGB_MODULE_PRIVATE_VAR rgb::assert_callback callback = nullptr;

namespace rgb
{
 void assert_handler( const sysc * expr, const sysc * file, const sysc * fun,
					  const nuint line, const sysc * msg )
 {
  if ( callback )
  {
   const nuint buffer_size = 1 << 12;
   utf8c buffer[buffer_size] = { 0 };
   utf8c * expr_u = nullptr,
	     * file_u = nullptr,
	     * fun_u = nullptr,
	     * msg_u = nullptr;

   nuint ret_all = 0,
         ret_tmp = 0;

   ret_tmp = cast( buffer + ret_all, buffer_size, expr, length( expr ) );
   if ( ret_tmp )
   {
    expr_u = buffer + ret_all;
    ret_all += ret_tmp + 1;
   }

   ret_tmp = cast( buffer + ret_all, buffer_size, file, length( file ) );
   if ( ret_tmp )
   {
    file_u = buffer + ret_all;
    ret_all += ret_tmp + 1;
   }

   ret_tmp = cast( buffer + ret_all, buffer_size, fun, length( fun ) );
   if ( ret_tmp )
   {
    fun_u = buffer + ret_all;
    ret_all += ret_tmp + 1;
   }

   if ( msg != nullptr )
   {
    ret_tmp = cast( buffer + ret_all, buffer_size, msg, length( msg ) );
    if ( ret_tmp )
    {
     msg_u = buffer + ret_all;
     ret_all += ret_tmp + 1;
    }
   }

   return callback( expr_u, file_u, fun_u, line, msg_u );
  } else
  {
  }
 }

 void assert_set( assert_callback call )
 {
  ::callback = call;
 }

 assert_callback assert_get( )
 {
  return ::callback;
 }
}
