#include "./../rgb/color.hpp"

namespace rgb
{
 const color_rgb color_rgb::Black( 0, 0, 0 );
 const color_rgb color_rgb::White( 255, 255, 255 );
 const color_rgb color_rgb::Red( 255, 0, 0 );
 const color_rgb color_rgb::Green( 0, 255, 0 );
 const color_rgb color_rgb::Blue( 0, 0, 255 );

 color_rgb::color_rgb( )
  : r( 0 ), g( 0 ), b( 0 )
 { }

 color_rgb::color_rgb( uint8 r, uint8 g, uint8 b )
  : r( r ), g( g ), b( b )
 { }
}
