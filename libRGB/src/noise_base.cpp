#include "./../rgb/noise_base.hpp"

#include "./../rgb/assert.hpp"
#include <limits>
#include <algorithm>
#include <stdarg.h>

namespace rgb
{
 noise_base::noise_base( uint32 u )
  : _seed( u )
 { }

 uint32 noise_base::get_seed( ) const
 {
  return _seed;
 }

 void noise_base::set_seed( uint32 s )
 {
  _seed = s;
 }

 noise_integral_base::noise_integral_base( uint32 seed )
  : noise_base( seed )
 { }

 noise_coherent_base::noise_coherent_base( quality q, uint32 seed )
  : noise_base( seed ), _quality( q )
 { }

 noise_coherent_base::quality noise_coherent_base::get_quality( ) const
 {
  return _quality;
 }

 void noise_coherent_base::set_quality( quality q )
 {
  _quality = q;
 }

 noise_combiner_base::noise_combiner_base( nuint count )
  : _noises( nullptr ), _size( count )
 {
  _noises = new noise_base*[_size];
 }

 noise_combiner_base::noise_combiner_base( nuint count, noise_base * ptr, ... )
  : _noises( nullptr ), _size( count )
 {
  _noises = new noise_base*[_size];

  set_noise( 0, ptr );

  count--;
  nuint it = 1;

  va_list lst;
  va_start( lst, ptr );

  while ( count-- > 0 )
  {
   noise_base * p = va_arg( lst, noise_base * );
   set_noise( it, p );
   it++;
  }

  va_end( lst );
 }

 noise_combiner_base::~noise_combiner_base( )
 {
  delete[ ] _noises;
 }

 noise_combiner_base::noise_combiner_base( noise_combiner_base && dying )
  : _noises( dying._noises ), _size( dying._size )
 {
  dying._noises = nullptr;
  dying._size = 0;
 }

 noise_combiner_base::noise_combiner_base( const noise_combiner_base & value )
  : _noises( nullptr ), _size( value._size )
 {
  if ( value._noises == nullptr )
   return;

  _noises = new noise_base*[_size];
  std::copy_n( value._noises, _size, _noises );
 }

 noise_combiner_base &noise_combiner_base::operator=( noise_combiner_base && dying )
 {
  if ( RGB_COMPARE_LHV( dying ) )
   return *this;

  delete[ ] _noises;

  _size = dying._size;
  _noises = dying._noises;

  dying._size = 0;
  dying._noises = nullptr;

  return *this;
 }

 noise_combiner_base & noise_combiner_base::operator=( const noise_combiner_base & value )
 {
  if ( RGB_COMPARE_LHV( value ) )
   return *this;

  delete[] _noises;

  if ( !value._size )
  {
   _noises = nullptr;
   _size = 0;
   return *this;
  }

  _size = value._size;
  _noises = new noise_base*[_size];

  std::copy_n( _noises, _size, value._noises );

  return *this;
 }

 void noise_combiner_base::set_noise( nuint it, noise_base * ptr )
 {
  RGB_FUNCTION_CONTRACT( it < _size );
  RGB_FUNCTION_CONTRACT( ptr != nullptr );

  _noises[it] = ptr;
 }

 noise_base * noise_combiner_base::get_noise( nuint it )
 {
  RGB_FUNCTION_CONTRACT( it < _size );

  return _noises[it];
 }

 noise_generator::noise_generator( uint32 seed )
  : noise_base( 0 )
 { }

 noise_generator::noise_generator( noise_base * base, interpolation_type nic,
                                   scurve_type ncc, bool delete_base,
                                   uint32 seed )
  : noise_base( seed ), _interpolation( nic ), _scurve( ncc ),
    _noise_generator( base ), _clean( delete_base )
 {
  RGB_FUNCTION_CONTRACT( base != nullptr );
 }

 noise_generator::~noise_generator( )
 {
  if ( _clean )
   delete _noise_generator;
 }

 /*
 noise_generator( noise_generator && dying );
 noise_generator( const noise_generator & value );

 noise_generator& operator=( noise_generator && dying );
 noise_generator& operator=( const noise_generator & value ); */

 float64 noise_generator::get( float64 x )
 {
  float64 d0, d1;

  d0 = _noise_generator->get( (int32)x );
  d1 = _noise_generator->get( (int32)x + 1 );

  float64 t = _scurve( x - (float64)(int32)x );

  return _interpolation( d0, d1, t );
 }

 float64 noise_generator::get( float64 x, float64 y )
 {
  float64 d00, d01, d10, d11;

  d00 = _noise_generator->get( (int32)x, (int32)y );
  d01 = _noise_generator->get( (int32)x + 1, (int32)y );
  d10 = _noise_generator->get( (int32)x, (int32)y + 1 );
  d11 = _noise_generator->get( (int32)x + 1, (int32)y + 1 );

  float64 tx, ty;

  tx = _scurve( x - (float64)(int32)x );
  ty = _scurve( y - (float64)(int32)y );

  float64 i1, i2;

  i1 = _interpolation( d00, d01, tx );
  i2 = _interpolation( d10, d11, tx );

  return _interpolation( i1, i2, ty );
 }

 float64 noise_generator::get( float64 x, float64 y, float64 z )
 {
  float64 d000, d001, d010, d011, d100, d101, d110, d111;

  d000 = _noise_generator->get( (int32)x, (int32)y, (int32)z );
  d001 = _noise_generator->get( (int32)x, (int32)y, (int32)z + 1 );
  d010 = _noise_generator->get( (int32)x, (int32)y + 1, (int32)z );
  d011 = _noise_generator->get( (int32)x, (int32)y + 1, (int32)z + 1 );
  d100 = _noise_generator->get( (int32)x + 1, (int32)y, (int32)z );
  d101 = _noise_generator->get( (int32)x + 1, (int32)y, (int32)z + 1 );
  d110 = _noise_generator->get( (int32)x + 1, (int32)y + 1, (int32)z );
  d111 = _noise_generator->get( (int32)x + 1, (int32)y + 1, (int32)z + 1 );

  float64 tx, ty, tz;

  tx = _scurve( x - (int32)x );
  ty = _scurve( y - (int32)y );
  tz = _scurve( z - (int32)z );

  float64 i00, i01, i10, i11;

  i00 = _interpolation( d000, d100, tx );
  i01 = _interpolation( d010, d110, tx );
  i10 = _interpolation( d001, d101, tx );
  i11 = _interpolation( d011, d111, tx );

  float64 c0, c1;

  c0 = _interpolation( i00, i01, tz );
  c1 = _interpolation( i10, i11, tz );

  return _interpolation( c0, c1, ty );
 }

 noise_generator::interpolation_type noise_generator::get_interpolation( ) const
 {
  return _interpolation;
 }

 void noise_generator::set_interpolation( interpolation_type it )
 {
  RGB_FUNCTION_CONTRACT( it != nullptr );

  _interpolation = it;
 }

 noise_generator::scurve_type noise_generator::get_scurve( ) const
 {
  return _scurve;
 }

 void noise_generator::set_scurve( scurve_type st )
 {
  RGB_FUNCTION_CONTRACT( st != nullptr );

  _scurve = st;
 }
}
