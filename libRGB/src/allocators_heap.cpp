#include "./../rgb/allocators_heap.hpp"

#include "./../rgb/logger_macros.hpp"
#include "./../rgb/internal/winapi-include.hpp"
#include <algorithm>

namespace rgb
{
 allocator_heap::allocator_heap( allocator_heap && dying )
  : allocator( std::move( dying ) )
 { }

 allocator_heap &allocator_heap::operator = ( allocator_heap && dying )
 {
  allocator::operator=( std::move( dying ) );
  return *this;
 }

 allocator::memory_block allocator_heap::allocate( nuint count )
 {
  HANDLE process_heap = INVALID_HANDLE_VALUE;
  memory_block mem_blk = nullptr;

  RGB_LOG_ON_FAILURE( "Can't allocate ${} bytes", count );

  RGB_LOG_SYSTEM_AR( process_heap, == INVALID_HANDLE_VALUE, nullptr,
                     GetProcessHeap( ) );
  RGB_LOG_SYSTEM_AR( mem_blk, == nullptr, nullptr,
                     HeapAlloc( process_heap, 0, count ) );

  RGB_LOG_CLEAR_FAILURE( );

  return mem_blk;
 }

 void allocator_heap::deallocate( memory_block m, nuint count )
 {
  // HeapFree nie lubi nullptr jako argumentu
  // msdn: https://msdn.microsoft.com/en-us/library/windows/desktop/aa366701%28v=vs.85%29.aspx
  //  If this pointer is NULL, the behavior is undefined.
  if ( m == nullptr )
   return;

  HANDLE process_heap = INVALID_HANDLE_VALUE;

  RGB_LOG_SYSTEM_AR( process_heap, == INVALID_HANDLE_VALUE, ,
                     GetProcessHeap( ) );
  RGB_LOG_SYSTEM_R( == FALSE, , HeapFree( process_heap, 0, m ) );
 }
}

namespace rgb
{
 allocator_heap_lock::allocator_heap_lock( allocator_heap_lock && dying )
  : allocator_heap( std::move( dying ) )
 { }

 allocator_heap_lock &allocator_heap_lock::operator = ( allocator_heap_lock && dying )
 {
  allocator_heap::operator=( std::move( dying ) );
  return *this;
 }

 allocator::memory_block allocator_heap_lock::allocate( nuint count )
 {
  memory_block mblk = allocator_heap::allocate( count );

  if ( mblk == nullptr )
  {
   RGB_LOG_ERROR( "Can't allocate ${} bytes", count );
   return nullptr;
  }

  RGB_LOG_SYSTEM_N(BOOL, is, == FALSE, VirtualLock( mblk, count ));

  if ( is == TRUE )
  {
   RGB_LOG_ERROR( "Can't lock ${} bytes in memory ${ptr}", count, mblk );
   allocator_heap::deallocate( mblk, count );
   return nullptr;
  }

  return mblk;
 }

 void allocator_heap_lock::deallocate( memory_block m, nuint count )
 {
  if ( m == nullptr )
   return;

  RGB_LOG_SYSTEM_R( == FALSE, , VirtualUnlock( m, count ) );

  return allocator_heap::deallocate( m, count );
 }
}

namespace rgb
{
 allocator_heap_clean::allocator_heap_clean( allocator_heap_clean && dying )
  : allocator_heap( std::move( dying ) )
 { }

 allocator_heap_clean &allocator_heap_clean::operator = ( allocator_heap_clean && dying )
 {
  allocator_heap::operator=( std::move( dying ) );
  return *this;
 }

 allocator::memory_block allocator_heap_clean::allocate( nuint count )
 {
  return allocator_heap::allocate( count );
 }

 void allocator_heap_clean::deallocate( memory_block m, nuint count )
 {
  if ( m == nullptr )
   return;

  byte * p = reinterpret_cast<byte*>( m );

  std::fill_n( p, count, 0 );

  return allocator_heap::deallocate( m, count );
 }
}

namespace rgb
{
 allocator_heap_clean_lock::allocator_heap_clean_lock( allocator_heap_clean_lock && dying )
  : allocator_heap_lock( std::move( dying ) )
 { }

 allocator_heap_clean_lock &allocator_heap_clean_lock::operator = ( allocator_heap_clean_lock && dying )
 {
  allocator_heap_lock::operator=( std::move( dying ) );
  return *this;
 }

 allocator::memory_block allocator_heap_clean_lock::allocate( nuint count )
 {
  return allocator_heap_lock::allocate( count );
 }

 void allocator_heap_clean_lock::deallocate( memory_block m, nuint count )
 {
  if ( m == nullptr )
   return;

  byte * p = reinterpret_cast<byte*>( m );

  std::fill_n( p, count, 0 );

  return allocator_heap_lock::deallocate( m, count );
 }
}
