#include "./../rgb/time_timespans.hpp"

namespace rgb
{
 time_span time_span::from_nanosec( uint64 ns )
 {
  return time_span( ns );
 }

 time_span time_span::from_microsec( uint64 us )
 {
  return time_span( us * ( constants::nanosecond_denominator /
                           constants::microsecond_denominator )
                  );
 }

 time_span time_span::from_millisec( uint32 ms )
 {
  uint64 t = ms;

  return time_span( t * ( constants::nanosecond_denominator /
                          constants::millisecond_denominator )
                  );
 }

 time_span time_span::from_seconds( uint32 s )
 {
  uint64 t = s;

  return time_span( t * ( constants::nanosecond_denominator /
                          constants::seconds_denominator )
                  );
 }

 time_span time_span::from_seconds( float64 s )
 {
  uint64  i = uint64(float80(s) * ( constants::nanosecond_denominator /
                                    constants::seconds_denominator ));

  return time_span( i );
 }

 time_span::time_span( uint64 ns )
  : _interval( ns )
 { }

 time_span::time_span( const time_span & value )
  : _interval( value._interval )
 { }

 time_span::time_span( time_span && dying )
  : _interval( dying._interval )
 { }

 time_span::~time_span( )
 { }

 time_span &time_span::operator= ( const time_span & value )
 {
  _interval = value._interval;
  return *this;
 }

 time_span &time_span::operator=( time_span && dying )
 {
  _interval = dying._interval;
  return *this;
 }

 uint64 time_span::get_nano( ) const
 {
  return _interval;
 }

 uint64 time_span::get_micro( ) const
 {
  return _interval / ( constants::nanosecond_denominator /
                       constants::microsecond_denominator );
 }

 uint64 time_span::get_milli( ) const
 {
  return _interval / ( constants::nanosecond_denominator /
                       constants::millisecond_denominator );
 }

 uint64 time_span::get_sec_i( ) const
 {
  return _interval / ( constants::nanosecond_denominator /
                       constants::seconds_denominator );
 }

 float64 time_span::get_sec( ) const
 {
  return float64( _interval ) / ( constants::nanosecond_denominator /
                                  constants::seconds_denominator );
 }

 bool operator==( const time_span & left, const time_span & right )
 {
  return left._interval == right._interval;
 }

 bool operator!=( const time_span & left, const time_span & right )
 {
  return left._interval != right._interval;
 }

 bool operator> ( const time_span & left, const time_span & right )
 {
  return left._interval > right._interval;
 }

 bool operator>=( const time_span & left, const time_span & right )
 {
  return left._interval >= right._interval;
 }

 bool operator< ( const time_span & left, const time_span & right )
 {
  return left._interval < right._interval;
 }

 bool operator<=( const time_span & left, const time_span & right )
 {
  return left._interval <= right._interval;
 }
}
