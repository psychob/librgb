﻿//
// libRGB
// (c) 2015 Andrzej Budzanowski
//

#include "./libRGB.hpp"
#include "./types.hpp"

#include <new>     // dla placement new
#include <utility> // dla std::forward

#pragma once

//!
//! \file rgb/allocators_pimp.hpp
//! \brief W tym pliku znajdują się wszystkie funkcje pomocnicze potrzebne w
//!  implementacji wzorca pimpl (w bibliotece nazywanego pimp).
//! \author Andrzej Budzanowski <psychob.pl@gmail.com>
//!
//! \sa \link page_using_pimpl Używanie wzorca pimpl (Private IMPLementation)
//!  \endlink
//!

namespace rgb
{
 //!
 //! \brief Funkcja szablonowa pozwalająca na inicjalizacjie prywantnej
 //!  zmiennej.
 //! \tparam T Typ ukrytej zmiennej
 //! \tparam N Wielkość zmiennej \em arr (nie powinieneś tego parametru
 //!  ustawiać ręcznie).
 //! \param [in,out] arr Zmienna do której jest inicjalizowana "ukryta" zmienna
 //!  o typie T
 //! \param [in] a Argumenty przekazywane do kontruktora
 //! \ingroup group_allocators
 //!
 template < typename T, size_t N, typename... Args >
  void pimp_init( byte( &arr )[N], Args ...a )
  {
   static_assert( sizeof( T ) <= N,
                  "bufor nie jest tak wielki by pomieścić obiekt T" );

   new (arr)T( std::forward<Args>(a)... );
  }

 //!
 //! \brief Funkcja szablonowa pozwalająca na deinicjalizacje prywatnej
 //!  zmiennej.
 //! \tparam T typ ukrytej zmiennej.
 //! \tparam N Wielkość zmiennej \em arr (nie powinieneś tego parametru
 //!  ustawiać ręcznie).
 //! \param [in] arr Zmienna z której jest usuwana "ukryta" zmienna.
 //! \ingroup group_allocators
 //!
 template < typename T, size_t N >
  void pimp_deinit( byte( &arr )[N] )
  {
   static_assert( sizeof( T ) <= N,
                  "bufor nie jest tak wielki by pomieścić obiekt T" );

   T * ptr = reinterpret_cast<T*>( arr );

   if ( ptr == nullptr )
    return;

   ptr->~T( );
  }

 //!
 //! \brief Funkcja pobiera ukrytą zmienna o typie \em T.
 //! \tparam T Typ ukrytej zmiennej
 //! \tparam N Wielkość zmiennej \em arr (nie powinieneś tego parametru
 //!  ustawiać ręcznie)
 //! \param [in] arr Zmienna z której jest pobierana "ukryta" zmienna.
 //! \return Wskaźnik na zmienną o typie \em T.
 //!
 //! \remarks
 //!  Ta funkcja jest przeciążona by pobierać stałą ukrytą zmienną ze stałego
 //!  bufora.
 //! \ingroup group_allocators
 //!
 template < typename T, typename size_t N >
  T * pimp_get( byte( &arr )[N] )
  {
   static_assert( sizeof( T ) <= N,
                  "bufor nie jest tak wielki by pomieścić obiekt T" );

   return reinterpret_cast<T*>( arr );
  }

#ifndef __DOXYGEN__
 template < typename T, typename size_t N >
  const T * pimp_get( const byte( &arr )[N] )
  {
   static_assert( sizeof( T ) <= N,
                  "bufor nie jest tak wielki by pomieścić obiekt T" );

   return reinterpret_cast<const T*>( arr );
  }

 template < typename T, size_t NI, size_t NO >
  typename std::enable_if< (std::is_pod< T >::value) &&
                            std::is_copy_constructible<T>::value,
                           void  >::type
   pimp_copyc( byte( &to )[NI], const byte( &from )[NO] )
   {
    static_assert( sizeof( T ) <= NI && sizeof( T ) <= NO,
                   "bufor nie jest tak wielki by pomieścić obiekt T" );

    memcpy( to, from, sizeof( T ) );
   }
#endif // __DOXYGEN__

 //!
 //! \brief Funkcja konstruuje jeden obiekt kopiując zawartość drugiego.
 //! \tparam T Typ ukrytej zmiennej
 //! \tparam NI,NO Wielkość zmiennych \em to i \em from. (Nie powinieneś tych
 //!  parametrów ustawiać ręcznie)
 //! \param [in] to Do tej zmiennej zostanie skonstruowany nowy obiekt kopiując
 //!  obiekt \em from
 //! \param [in] from Z tej zmiennej zostanie pobrana ukryta zmienna.
 //!
 //! \remarks
 //!  Ta funkcja posiada przeładowanie które jest uruchamiane jeśli obiekt \em
 //!  T jest typu POD (\em Plain \em Old \em Data) to zamiast odpalania jego
 //!  konstruktora kopiującego, zawartość jest kopiowana przy pomocy \em
 //!  memcpy.
 //!
 //! \remarks
 //!  Ta funkcja nie istnieje dla typów które nie mogą być utworzone przy
 //!  pomocy konstruktora kopiującego.
 //! \ingroup group_allocators
 //!
 template < typename T, size_t NI, size_t NO >
#ifndef __DOXYGEN__
  typename std::enable_if< !(std::is_pod< T >::value) &&
                             std::is_copy_constructible<T>::value,
                           void >::type
#else
 void
#endif // __DOXYGEN__
   pimp_copyc( byte( &to )[NI], const byte( &from )[NO] )
   {
    static_assert( sizeof( T ) <= NI && sizeof( T ) <= NO,
                   "bufor nie jest tak wielki by pomieścić obiekt T" );

    auto from_ptr = pimp_get<T>( from );

    pimp_init<T>( to, *from_ptr );
   }

 //!
 //! \brief Funkcja przesuwajaca jeden ukryty obiekt do drugiego przy użyciu
 //!  konstruktora przesuwającego.
 //! \tparam T Typ ukrytego obiektu
 //! \tparam NI Wielkość parametru \em to (Nie powienieneś tego ustawiać sam)
 //! \tparam NO Wielkość parametru \em from (Nie powinieneś tego ustawiać sam)
 //! \param [out] to Parametr do którego zostanie przesunięta wartość ukrytego
 //!  obiektu.
 //! \param [in] from Parametr z którego zostanie przesunięta wartość ukrytego
 //!  obiektu.
 //! \todo 27/05/2015 Dodanie optymalizacji dla POD
 //! \ingroup group_allocators
 //!
 template < typename T, size_t NI, size_t NO >
  typename std::enable_if< std::is_move_constructible<T>::value, void>::type
   pimp_movec( byte( &to )[NI], byte( &from )[NO] )
   {
    static_assert( sizeof( T ) <= NI && sizeof( T ) <= NO,
                   "bufor nie jest tak wielki by pomieścić obiekt T" );

    auto from_ptr = pimp_get<T>( from );

    pimp_init<T>( to, std::move( *from_ptr ) );

    memset( from, 0, NO );
   }

 //!
 //! \brief Funkcja kopiująca ukryty obiekt przy pomocy operatora kopiującego.
 //! \tparam T Typ ukrytego obiektu
 //! \tparam NI Wielkość parametru \em to (Nie powienieneś tego ustawiać sam)
 //! \tparam NO Wielkość parametru \em from (Nie powinieneś tego ustawiać sam)
 //! \param [out] to Parametr do którego zostanie skopiowana wartość ukrytego
 //!  obiektu.
 //! \param [in] from Parametr z którego zostanie skopiowana wartość ukrytego
 //!  obiektu.
 //! \todo 27/05/2015 Dodanie optymalizacji dla POD
 //! \ingroup group_allocators
 //!
 template < typename T, size_t NI, size_t NO >
  typename std::enable_if< std::is_copy_assignable<T>::value, void>::type
   pimp_copya( byte( &to )[NI], const byte( &from )[NO] )
   {
    static_assert( sizeof( T ) <= NI && sizeof( T ) <= NO,
                   "bufor nie jest tak wielki by pomieścić obiekt T" );

    auto from_ptr = pimp_get<T>( from );
    auto to_ptr   = pimp_get<T>( to );

    to_ptr->operator=( *from_ptr );
   }

 //!
 //! \brief Funkcja przesuwajaca jeden ukryty obiekt do drugiego, przy użyciu
 //!  operatora przesuwającego.
 //! \tparam T Typ ukrytego obiektu
 //! \tparam NI Wielkość parametru \em to (Nie powienieneś tego ustawiać sam)
 //! \tparam NO Wielkość parametru \em from (Nie powinieneś tego ustawiać sam)
 //! \param [out] to Parametr do którego zostanie przesunięta wartość ukrytego
 //!  obiektu.
 //! \param [in] from Parametr z którego zostanie przesunięta wartość ukrytego
 //!  obiektu.
 //! \todo 27/05/2015 Dodanie optymalizacji dla POD
 //! \ingroup group_allocators
 //!
 template < typename T, size_t NI, size_t NO >
  typename std::enable_if< std::is_move_assignable<T>::value, void>::type
   pimp_movea( byte( &to )[NI], byte( &from )[NO] )
   {
    static_assert( sizeof( T ) <= NI && sizeof( T ) <= NO,
                   "bufor nie jest tak wielki by pomieścić obiekt T" );

    auto from_ptr = pimp_get<T>( from );
    auto to_ptr = pimp_get<T>( to );

    to_ptr->operator=( std::move(*from_ptr) );

    memset( from, 0, NO );
   }
}
