//
// libRGB
// (c) 2015 Andrzej Budzanowski
//

#include "./libRGB.hpp"
#include "./time_timespans.hpp"

#pragma once

namespace rgb
{
 //!
 //! \brief
 //!  Funkcja usypia w�tek na okre�lon� ilo�� czasu.
 //! \param [in] ts Na ile czasu w�tek ma by� u�piony
 //! \warning W zale�no�ci od systemu operacyjnego, funkcja mo�e odczaka�
 //!  troch� mniej albo troch� wi�cej czasu.
 //! \todo Oddzieli� odpalenie funkcji boostuj�cej clock resolution timer na
 //!  inne?
 //!
 RGB_API RGB_NOTHROW
  void sleep( const time_span & ts );
}
