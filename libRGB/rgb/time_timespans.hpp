﻿//
// libRGB
// (c) 2015 Andrzej Budzanowski
//

#include "./libRGB.hpp"
#include "./types.hpp"

#pragma once

namespace rgb
{
 namespace constants
 {
  const static uint64 seconds_denominator     = 1;          // 10^0
  const static uint64 decisecond_denominator  = 10;         // 10^-1
  const static uint64 centisecond_denominator = 100;        // 10^-2
  const static uint64 millisecond_denominator = 1000;       // 10^-3
  const static uint64 microsecond_denominator = 1000000;    // 10^-6
  const static uint64 nanosecond_denominator  = 1000000000; // 10^-9
 }

 struct RGB_API time_span
 {
  public:
   // inicjalizatory
   static time_span from_nanosec ( uint64 ns );
   static time_span from_microsec( uint64 us );
   static time_span from_millisec( uint32 ms );
   static time_span from_seconds ( uint32 s );
   static time_span from_seconds ( float64 s );

  private:
   uint64 _interval;
   time_span( uint64 _interval );

  public:
   time_span( const time_span & value );
   time_span( time_span && dying );

   ~time_span( );

   time_span &operator=( const time_span & value );
   time_span &operator=( time_span && dying );

   uint64 get_nano( ) const;
   uint64 get_micro( ) const;
   uint64 get_milli( ) const;
   uint64 get_sec_i( ) const;
   float64 get_sec( ) const;

   RGB_API RGB_CONST RGB_NOTHROW
    friend bool operator==( const time_span & left, const time_span & right );
   RGB_API RGB_CONST RGB_NOTHROW
    friend bool operator!=( const time_span & left, const time_span & right );
   RGB_API RGB_CONST RGB_NOTHROW
    friend bool operator> ( const time_span & left, const time_span & right );
   RGB_API RGB_CONST RGB_NOTHROW
    friend bool operator>=( const time_span & left, const time_span & right );
   RGB_API RGB_CONST RGB_NOTHROW
    friend bool operator< ( const time_span & left, const time_span & right );
   RGB_API RGB_CONST RGB_NOTHROW
    friend bool operator<=( const time_span & left, const time_span & right );
 };
}