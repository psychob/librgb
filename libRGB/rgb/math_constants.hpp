//
// libRGB
// (c) 2015 Andrzej Budzanowski
//

#include "./libRGB.hpp"
#include "./types.hpp"

#pragma once

namespace rgb
{
 namespace constants
 {
  static const float80 PI = 3.14;
 }
}
