//
// libRGB
// (c) 2015 Andrzej Budzanowski
//

#include "./libRGB.hpp"
#include "./types.hpp"
#include "./noise_base.hpp"

#pragma once

namespace rgb
{
 class RGB_API noise_integral : public noise_integral_base
 {
  public:
   noise_integral( uint32 seed = 0 );

   RGB_CONST RGB_NOTHROW
    float64 get( float64 x ) override;
   RGB_CONST RGB_NOTHROW
    float64 get( float64 x, float64 y ) override;
   RGB_CONST RGB_NOTHROW
    float64 get( float64 x, float64 y, float64 z ) override;

   RGB_CONST RGB_NOTHROW
    float64 get( int32 x ) override;
   RGB_CONST RGB_NOTHROW
    float64 get( int32 x, int32 y ) override;
   RGB_CONST RGB_NOTHROW
    float64 get( int32 x, int32 y, int32 z ) override;
 };

 RGB_API RGB_CONST RGB_NOTHROW
  float64 noise_integral_generator( int32 x, uint32 seed = 0 );
 RGB_API RGB_CONST RGB_NOTHROW
  float64 noise_integral_generator( int32 x, int32 y, uint32 seed = 0 );
 RGB_API RGB_CONST RGB_NOTHROW
  float64 noise_integral_generator( int32 x, int32 y, int32 z,
                                    uint32 seed = 0 );

 class RGB_API noise_coherent : public noise_coherent_base
 {
  public:
   noise_coherent( quality q = quality::normal, uint32 seed = 0 );

  RGB_CONST RGB_NOTHROW
   float64 get( float64 x ) override;
  RGB_CONST RGB_NOTHROW
   float64 get( float64 x, float64 y ) override;
  RGB_CONST RGB_NOTHROW
   float64 get( float64 x, float64 y, float64 z ) override;
 };

 RGB_API RGB_CONST RGB_NOTHROW
  float64 noise_coherent_generator( float64 x,
                                    noise_coherent_base::quality q =
                                     noise_coherent_base::quality::normal,
                                    uint32 seed = 0 );
 RGB_API RGB_CONST RGB_NOTHROW
  float64 noise_coherent_generator( float64 x, float64 y,
                                    noise_coherent_base::quality q =
                                     noise_coherent_base::quality::normal,
                                    uint32 seed = 0 );
 RGB_API RGB_CONST RGB_NOTHROW
  float64 noise_coherent_generator( float64 x, float64 y, float64 z,
                                    noise_coherent_base::quality q =
                                     noise_coherent_base::quality::normal,
                                    uint32 seed = 0 );
 RGB_API RGB_CONST RGB_NOTHROW
  float64 noise_coherent_generator( float64 fx, int32 ix,
                                    noise_coherent_base::quality q =
                                     noise_coherent_base::quality::normal,
                                    uint32 seed = 0 );
 RGB_API RGB_CONST RGB_NOTHROW
  float64 noise_coherent_generator( float64 fx, float64 fy, int32 ix, int32 iy,
                                    noise_coherent_base::quality q =
                                     noise_coherent_base::quality::normal,
                                    uint32 seed = 0 );
 RGB_API RGB_CONST RGB_NOTHROW
  float64 noise_coherent_generator( float64 fx, float64 fy, float64 fz,
                                    int32 ix, int32 iy, int32 iz,
                                    noise_coherent_base::quality q =
                                     noise_coherent_base::quality::normal,
                                    uint32 seed = 0 );
}
