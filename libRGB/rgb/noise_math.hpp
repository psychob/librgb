//
// libRGB
// (c) 2015 Andrzej Budzanowski
//

#include "./libRGB.hpp"
#include "./types.hpp"
#include "./math_constants.hpp"

#pragma once

namespace rgb
{
 template < typename Float >
  using noise_interpolation_callback = Float( *)( Float a, Float b, Float t );

 template < typename Float >
  using noise_curve_callback = Float( *)( Float x );

 template < typename T >
  static inline T clamp( T value, const T min, const T max )
  {
   return ( value >= max ? max :
            value <= min ? min : value );
  }

 //! @see http://paulbourke.net/miscellaneous/interpolation/
 //! @see http://en.wikipedia.org/wiki/Linear_interpolation#Programming_language_support
 template < typename Float >
  static inline Float lerp( Float a, Float b, Float t )
  {
   return a + t * ( b - a );
  }

 //! @see http://paulbourke.net/miscellaneous/interpolation/
 template < typename Float >
  static inline Float cosine( Float a, Float b, Float t )
  {
   Float tmp = ( 1 - cos( t*constants::PI ) ) / 2;
   return ( a * ( 1 - tmp ) + b * tmp );
  }

 //! @see http://en.wikipedia.org/wiki/Smoothstep
 template < typename Float >
  static inline Float smoothstep( Float a, Float b, Float t )
  {
   t = clamp<Float>( ( t - a ) / ( b - a ), 0.0, 1.0 );
   return t * t * ( 3 - 2 * t );
  }

 //! @see http://codeplea.com/simple-interpolation
 template < typename Float >
  static inline Float acceleration( Float a, Float b, Float t )
  {
   return lerp<Float>( a, b, t*t );
  }

 //! @see http://codeplea.com/simple-interpolation
 template < typename Float >
  static inline Float deceleration( Float a, Float b, Float t )
  {
   return lerp<Float>( a, b, 1 - ( 1 - t ) * ( 1 - t ) );
  }

 template < typename Float >
  static inline Float scurve0( Float x )
  {
   return Float(0);
  }

 template < typename Float >
  static inline Float scurve3( Float x )
  {
   return -2 * ( x * x * x ) + 3 * ( x * x );
  }

 template < typename Float >
  static inline Float scurve5( Float x )
  {
   return 6 * ( x * x * x * x * x ) - 15 * ( x *x * x * x ) +
          10 * ( x * x * x );
  }
}
