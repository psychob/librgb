﻿//
// libRGB
// (c) 2015 Andrzej Budzanowski
//

#include "./libRGB.hpp"
#include "./types.hpp"
#include "./allocators_interface.hpp"

#pragma once

namespace rgb
{
 //!
 //! \brief
 //!  Alokator który zwraca pamięć ze sterty (heap).
 //! \todo 30/06/2015: Dodanie zwalniania n-bajtów pamięci
 //! \author Andrzej Budzanowski <psychob.pl@gmail.com>
 //! \ingroup group_allocators
 //!
 class RGB_API allocator_heap : public allocator
 {
  public:
   allocator_heap( ) = default;
   virtual ~allocator_heap( ) = default;

   allocator_heap( const allocator_heap & ) = default;
   allocator_heap& operator=( const allocator_heap & ) = default;

   allocator_heap( allocator_heap && dying );
   allocator_heap& operator=( allocator_heap && dying );

   memory_block allocate( nuint count ) override;
   void deallocate( memory_block m, nuint count ) override;
 };

 //!
 //! \brief
 //!  Alokator zwraca pamięć ze sterty i blokuje ją w fizycznym RAMie.
 //! \todo 30/06/2015: Dodanie zwalniania n-bajtów pamięci
 //! \author Andrzej Budzanowski <psychob.pl@gmail.com>
 //! \ingroup group_allocators
 //!
 class RGB_API allocator_heap_lock : public allocator_heap
 {
  public:
   allocator_heap_lock( ) = default;
   virtual ~allocator_heap_lock( ) = default;

   allocator_heap_lock( const allocator_heap_lock & ) = default;
   allocator_heap_lock& operator=( const allocator_heap_lock & ) = default;

   allocator_heap_lock( allocator_heap_lock && dying );
   allocator_heap_lock& operator=( allocator_heap_lock && dying );

   memory_block allocate( nuint count ) override;
   void deallocate( memory_block m, nuint count ) override;
 };

 //!
 //! \brief
 //!  Alokator zwraca pamięć ze sterty, a gdy pamięć ma byc uwolniona do
 //!  systemu, nadpisuje ją zerami.
 //! \todo 30/06/2015: Dodanie zwalniania n-bajtów pamięci
 //! \author Andrzej Budzanowski <psychob.pl@gmail.com>
 //! \ingroup group_allocators
 //!
 class RGB_API allocator_heap_clean : public allocator_heap
 {
  public:
   allocator_heap_clean( ) = default;
   virtual ~allocator_heap_clean( ) = default;

   allocator_heap_clean( const allocator_heap_clean & ) = default;
   allocator_heap_clean& operator=( const allocator_heap_clean & ) = default;

   allocator_heap_clean( allocator_heap_clean && dying );
   allocator_heap_clean& operator=( allocator_heap_clean && dying );

   memory_block allocate( nuint count ) override;
   void deallocate( memory_block m, nuint count ) override;
 };

 //!
 //! \brief
 //!  Połączenie alokatorów allocator_heap_lock i allocator_heap_clean.
 //! \todo 30/06/2015: Dodanie zwalniania n-bajtów pamięci
 //! \author Andrzej Budzanowski <psychob.pl@gmail.com>
 //! \ingroup group_allocators
 //!
 class RGB_API allocator_heap_clean_lock : public allocator_heap_lock
 {
  public:
   allocator_heap_clean_lock( ) = default;
   virtual ~allocator_heap_clean_lock( ) = default;

   allocator_heap_clean_lock( const allocator_heap_clean_lock & ) = default;
   allocator_heap_clean_lock& operator=( const allocator_heap_clean_lock & ) = default;

   allocator_heap_clean_lock( allocator_heap_clean_lock && dying );
   allocator_heap_clean_lock& operator=( allocator_heap_clean_lock && dying );

   memory_block allocate( nuint count ) override;
   void deallocate( memory_block m, nuint count ) override;
 };
}
