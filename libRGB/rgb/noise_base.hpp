//
// libRGB
// (c) 2015 Andrzej Budzanowski
//

#include "./libRGB.hpp"
#include "./types.hpp"
#include "./noise_math.hpp"

#pragma once

namespace rgb
{
 class RGB_API noise_base
 {
  protected:
   uint32 _seed;

  public:
   noise_base( uint32 seed = 0 );
   virtual ~noise_base( ) = default;

   noise_base( const noise_base & ) = default;
   noise_base& operator=( const noise_base & ) = default;

   virtual float64 get( float64 x ) = 0;
   virtual float64 get( float64 x, float64 y ) = 0;
   virtual float64 get( float64 x, float64 y, float64 z ) = 0;

   uint32 get_seed( ) const;
   void set_seed( uint32 s );
 };

 class RGB_API noise_integral_base : public noise_base
 {
  public:
   noise_integral_base( uint32 seed = 0 );
   virtual ~noise_integral_base( ) = default;

   noise_integral_base( const noise_integral_base & ) = default;
   noise_integral_base& operator=( const noise_integral_base & ) = default;

   virtual float64 get( int32 x ) = 0;
   virtual float64 get( int32 x, int32 y ) = 0;
   virtual float64 get( int32 x, int32 y, int32 z ) = 0;
 };

 class RGB_API noise_coherent_base : public noise_base
 {
  public:
   enum class quality
   {
    low,
    normal,
    high,
   };

  protected:
   quality _quality;

  public:
   noise_coherent_base( quality q = quality::normal, uint32 seed = 0 );
   virtual ~noise_coherent_base( ) = default;

   noise_coherent_base( const noise_coherent_base & ) = default;
   noise_coherent_base& operator=( const noise_coherent_base & ) = default;

   quality get_quality( ) const;
   void set_quality( quality q );
 };

 class RGB_API noise_combiner_base : public noise_base
 {
  private:
   noise_base ** _noises;
   nuint _size;

  public:
   noise_combiner_base( nuint count );
   noise_combiner_base( nuint count, noise_base * ptr, ... );
   virtual ~noise_combiner_base( );

   noise_combiner_base( noise_combiner_base && );
   noise_combiner_base( const noise_combiner_base & );
   noise_combiner_base& operator=( noise_combiner_base && );
   noise_combiner_base& operator=( const noise_combiner_base & );

   void set_noise( nuint it, noise_base * ptr );
   noise_base * get_noise( nuint it );
 };

 class RGB_API noise_generator : public noise_base
 {
  public:
   using interpolation_type = noise_interpolation_callback<float64>;
   using scurve_type = noise_curve_callback<float64>;

  private:
   interpolation_type _interpolation = nullptr;
   scurve_type _scurve = nullptr;
   noise_base * _noise_generator = nullptr;
   bool _clean = false;

  public:
   noise_generator( uint32 seed = 0 );
   noise_generator( noise_base * base, interpolation_type nic, scurve_type ncc,
                    bool delete_base = false, uint32 seed = 0 );
   ~noise_generator( );

   noise_generator( noise_generator && dying );
   noise_generator( const noise_generator & value );

   noise_generator& operator=( noise_generator && dying );
   noise_generator& operator=( const noise_generator & value );

   float64 get( float64 x ) override;
   float64 get( float64 x, float64 y ) override;
   float64 get( float64 x, float64 y, float64 z ) override;

   interpolation_type get_interpolation( ) const;
   void set_interpolation( interpolation_type it );

   scurve_type get_scurve( ) const;
   void set_scurve( scurve_type st );
 };
}
