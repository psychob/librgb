﻿//
// libRGB
// (c) 2015 Andrzej Budzanowski
//

#include "./libRGB.hpp"
#include "./types.hpp"

#pragma once

namespace rgb
{
 //!
 //! \brief Struktura przechowująca kolor w formacie RGB.
 //! \author Andrzej Budzanowski <psychob.pl@gmail.com>
 //!
 struct RGB_API color_rgb
 {
  public:
   uint8 r, //!< Składnik Czerwony
         g, //!< Składnik Zielony
         b; //!< Składnik Niebieski

   //!
   //! \brief Konstruktor domyślny, inicjalizuje strukture na kolor czarny.
   //!
   color_rgb( );
   //!
   //! \brief Konstruktor inicjuje strukture na podany kolor
   //! \param [in] r Składowa czerwona
   //! \param [in] g Składowa zielona
   //! \param [in] b Składowa niebieska
   //!
   color_rgb( uint8 r, uint8 g, uint8 b );

   //! \brief Kolor czarny
   static const color_rgb Black;
   //! \brief Kolor biały
   static const color_rgb White;
   //! \brief Kolor czerwony
   static const color_rgb Red;
   //! \brief Kolor zielony
   static const color_rgb Green;
   //! \brief Kolor niebieski
   static const color_rgb Blue;
 };
}
