﻿//
// libRGB
// (c) 2015 Andrzej Budzanowski
//

#include "./libRGB.hpp"

#pragma once

//!
//! \page page_utf_char_and_utf_type_length Wielkość znaku a wielkość typu
//! \brief Którego typu należy używać przy zapisywaniu znaków w kodowaniu UTF.
//!
//! \author Andrzej Budzanowski <psychob.pl@gmail.com>
//!

//!
//! \file rgb/types.hpp
//! \brief W tym pliku są zdefiniowane wszystkie podstawowe typy.
//!
//! \author Andrzej Budzanowski <psychob.pl@gmail.com>
//!

//!
//! \brief Makrofunkcja zmieniająca c-stringa w sysc
//! \param [in] x c-string do zmiany
//! \remarks
//!  To makro zmienia tylko stałe napisy (czyli takie które są znane
//!  preporcesorowi w czasie kompilacji.
//! \todo 26/05/2015: Sprawdzanie czy potrzeba jakiejś konwersji
//!
#define RGB_SYSC_STR(x) ((const rgb::sysc *)(__LPREFIX(x)))
//!
//! \brief Makrofunkcja zmieniająca pojedyńczy znak w sysc
//! \param [in] x znak do zmiany
//! \remarks
//!  To makro zmienia tylko znaki znane w czasie kompilacji.
//! \todo 26/05/2015: Sprawdzanie czy potrzeba jakiejś konwersji
//!
#define RGB_SYSC_CHR(x) ((      rgb::sysc  )(__LPREFIX(x)))

namespace rgb
{
 //! \brief Typ całkowitoliczbowy ze znakiem mogący przechować 8 bitów (1
 //!  bajt).
 using int8 = __int8;
 //! \brief Typ całkowitoliczbowy ze znakiem mogący przechować 16 bitów (2
 //!  bajty).
 using int16 = __int16;
 //! \brief Typ całkowitoliczbowy ze znakiem mogący przechować 32 bity (4
 //!  bajty).
 using int32 = __int32;
 //! \brief Typ całkowitoliczbowy ze znakiem mogący przechować 64 bity (8
 //!  bajtów).
 using int64 = __int64;

 //! \brief Typ całkowitoliczbowy bez znaku mogący przechować 8 bitów (1
 //!  bajt).
 using uint8 = unsigned __int8;
 //! \brief Typ całkowitoliczbowy bez znaku mogący przechować 16 bitów (2
 //!  bajty).
 using uint16 = unsigned __int16;
 //! \brief Typ całkowitoliczbowy bez znaku mogący przechować 32 bity (4
 //!  bajty).
 using uint32 = unsigned __int32;
 //! \brief Typ całkowitoliczbowy bez znaku mogący przechować 64 bity (8
 //!  bajtów).
 using uint64 = unsigned __int64;

 //! \brief Typ naturalny dla procesora, ze znakiem. Dla platform 32 bitowych
 //!  jest 32 bitowych a dla 64 bitowych jest 64.
 using nint  = int32;
 //! \brief Typ naturalny dla procesora, bez znaku. Dla platform 32 bitowych
 //!  jest 32 bitowych a dla 64 bitowych jest 64.
 using nuint = uint32;

 //! \brief Typ zmiennoprzecinkowy o wielkości 32 bitów. Zapisany w formacie
 //!  IEEE 754
 using float32 = float;
 //! \brief Typ zmiennoprzecinkowy o wielkości 64 bitów. Zapisany w formacie
 //!  IEEE 754
 using float64 = double;
 //! \brief Typ zmiennoprzecinkowy o wielkości 80 bitów. Zapisany w formacie
 //!  IEEE 754
 using float80 = long double;

 //! \brief Typ przedstawiajacy jeden bajt. Używany jest on wtedy gdy mamy
 //! doczynienia z danymi binarnymi.
 using byte = uint8;

 //! \brief Typ znakowy przechowujący znaki w kodowaniu UTF-8.
 //! \sa \link page_utf_char_and_utf_type_length Długość znaku a wielkość typu
 //!  \endlink
 using utf8c  = uint8;
 //! \brief Typ znakowy przechowujący znaki w kodowaniu UTF-16.
 //! \sa \link page_utf_char_and_utf_type_length Długość znaku a wielkość typu
 //!  \endlink
 using utf16c = uint16;
 //! \brief Typ znakowy przechowujący znaki w kodowaniu UTF-32.
 //! \sa \link page_utf_char_and_utf_type_length Długość znaku a wielkość typu
 //!  \endlink
 using utf32c = uint32;
 //! \brief Typ znakowy przechowujący znaki w kodowaniu UTF.
 //! \sa \link page_utf_char_and_utf_type_length Długość znaku a wielkość typu
 //!  \endlink
 using utfcp  = utf32c;

 //! \brief Skrócona notacja dla typu wchar_t, bo pisanie dodatkowego _t jest
 //!  męczące
 using wchar = wchar_t;

 //!
 //! \brief Abstrakcja na typ znakowy naturalny dla systemu operacyjnego.
 //! \remarks
 //!  Dla systemów Linux typem standardowym jest char (najczęściej kodowany w
 //!  utf-8), a dla Windowsów jest to wchar_t (kodowany w UTF-16). By zapisywać
 //!  napisy w naturalnym dla systemu typie należy wykorzystać makra
 //!  #RGB_SYSC_CHR i #RGB_SYSC_STR
 //! \remarks
 //!  Z tej abstrackji opłaca się korzystać jeśli chcemy jak najszybszej
 //!  interakcji z systemem operacyjnym. Dlatego że biblioteka może działać z
 //!  dowolnym kodowaniem (char, wchar, utf8c, utf16c, utf32c), tylko musi
 //!  przekonwertować dane kodowanie jeśli system wymaga jakiegoś innego.
 //!
 using sysc  = wchar;
}
