﻿//
// libRGB
// (c) 2015 Andrzej Budzanowski
//

#include "./libRGB.hpp"
#include "./types.hpp"

#pragma once

namespace rgb
{
 //!
 //! \brief
 //!  W tej przestrzeni nazw znajdują się zdefiniowane stałe które mogą być
 //!  użyte do zwrócenia użytkownikowi wartości z funkcji `main`.
 //!
 //! Wartości dla poszczególnych kodów zostały zaczerpnięte z nagłówka
 //!  <sysexits.h> z systemu BSD.
 //!
 namespace exit_codes
 {
  enum
  {
   //! \brief
   //!  Wszystko jest ok
   ok = 0,
   //! \brief
   //!  Jakiś niezdefiniowany błąd
   failure,

   //! \brief
   //!  Program został źle użyty, zła liczba argumentów, zła flaga, zła
   //!  składnia w parametrach itp.
   usage = 64,
   //! \brief
   //!  Złe dane wejściowe wporwadzone do programu, to powinno być użyte tylko
   //!  dla plików użytkownika a nie dla plików systemu.
   input_data_error,
   //! \brief
   //!  Plik wejściowy nie istniał lub nie był odczytywalny, należy stosować to
   //!  tylko dla plików użytkownika nie dla plików systemowych
   no_input_data,
   //! \brief
   //!  Użytkownik dla tej akcji nie istnieje, dla przykładu w programie
   //!  pocztowym.
   no_user_exist,
   //! \brief
   //!  Host dla tej akcji nie istnieje, dla przykładu w programie pocztowym.
   no_host_exist,
   //! \brief
   //!  Usługa jest niedostępna. To może się zdarzyć jeśli program pomocniczy
   //!  lub plik nie istnieje. To może być użyte jako błąd typu 'catch-all',
   //!  jeśli to co chciałeś zrobić się nie powiodło, ale ty nie wiesz
   //!  dlaczego.
   unavailable,
   //! \brief
   //!  Wewnętrzny błąd programu został wykryty (nie powinno mieć to związku z
   //!  systemem), na przykład nieudana assercja itp.
   internal_software_error,
   //! \brief
   //!  Błąd systemu operacyjnego.
   os_error,
   //! \brief
   //!  Jakiś plik systemu operacyjnego nie istnieje, nie może być otworzony
   //!  albo jakiś inny błąd (błąd składni np)
   os_file_error,
   //! \brief
   //!  Plik wyjściowy nie może zostać utworzony
   output_cant_create,
   //! \brief
   //!  Jakiś inny błąd systemu I/O
   io_error,
   //! \brief
   //!  Tymczasowy błąd, czyli coś co nie jest tak naprawdę błędem tylko tak
   //!  naprawdę informacją że aktualnie czegoś nie można zrobić i należy
   //!  spróbować później.
   temporary_fail,
   //! \brief
   //!  Zdalny system zwrócił coś co było "niemożliwe" podczas wymiany
   //!  protokołowej.
   protocol_error,
   //! \brief
   //!  Użytkownik nie ma prawa do wykonania takiej operacji.
   no_premissions,
   //! \brief
   //!  Coś w konfiguracji jest nie-skonfigurowane lub w jakimś źle
   //!  skonfigurowanym stanie.
   bad_config,
  };
 }
}
