﻿//
// libRGB
// (c) 2015 Andrzej Budzanowski
//

#pragma once

//!
//! \file rgb/libRGB.hpp
//! \brief Główny plik biblioteki który musi być dołączany do każdego pliku
//!  biblioteki.
//! \author Andrzej Budzanowski <psychob.pl@gmail.com>
//!

// sprawdzanie który kompilator będzie kompilował naszą bibliotekę
#include "./internal/compiler-detect.hpp"

// definicja podstawowych makr
#include "./internal/macro-basic.hpp"

// definicja podstawowych makrofunkcji
#include "./internal/macro-func.hpp"

// definiowanie wszystkich pozostałych makr które mogą nam się przydać w
// krucjacie
#include "./internal/define-all-macros.hpp"

// sprawdzanie które dodatkowe opcje zostały zdefiniowane przy kompilowaniu
// biblioteki
#include "./internal/library-options.hpp"
