//
// libRGB
// (c) 2015 Andrzej Budzanowski
//

#include "./libRGB.hpp"
#include "./types.hpp"
#include "./allocators_interface.hpp"
#include <type_traits>
#include <new>

#pragma once

namespace rgb
{
 template < typename T, typename... Args >
  T * alloc_one( allocator * alloc, Args... a )
  {
   allocator::memory_block mb = alloc->allocate( sizeof( T ) );

   new (mb)T( std::forward<Args>( a )... );

   return reinterpret_cast<T*>( mb );
  }

 template < typename T >
  typename std::enable_if< std::is_trivially_destructible<T>::value, void >::type
   destruct_array( T * ptr, nuint count )
   { }

 template < typename T >
  typename std::enable_if< !std::is_trivially_destructible<T>::value, void >::type
   destruct_array( T * ptr, nuint count )
   {
    for ( nuint it = 0; it < count; ++it )
     ptr[it].~T( );
   }

 template < typename T >
  void dealloc_array( allocator * alloc, T * ptr, nuint count )
  {
   destruct_array( ptr, count );

   alloc->deallocate( ptr, count * sizeof( T ) );
  }

 RGB_API
  allocator * allocator_get( );
}
