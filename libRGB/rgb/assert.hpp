//
// libRGB
// (c) 2015 Andrzej Budzanowski
//

#include "./libRGB.hpp"
#include "./types.hpp"

#pragma once

#define RGB_INTERNAL_ASSERT(x,m)                                              \
 ( !!( x ) ? (void)0 : ::rgb::assert_handler( RGB_SYSC_STR( #x ),             \
                                              RGB_SYSC_STR( RGB_FILE ),       \
                                              RGB_SYSC_STR(RGB_FUNCTION),     \
                                              RGB_LINE, m ) )

#define RGB_ASSERT(x)            RGB_INTERNAL_ASSERT(x, nullptr)
#define RGB_ASSERT_MSG(x,y)      RGB_INTERNAL_ASSERT(x, RGB_SYSC_STR(y))
#define RGB_FUNCTION_CONTRACT(x) RGB_ASSERT_MSG(x, "Function contract broken")

namespace rgb
{
 using assert_callback = void( *)( const utf8c * expr, const utf8c * file,
								   const utf8c * fun, const nuint line,
								   const utf8c * msg );

 RGB_API RGB_COLD
  void assert_handler( const sysc * expr, const sysc * file, const sysc * fun,
                       const nuint line, const sysc * msg = nullptr );

 RGB_API RGB_NOTHROW
  void assert_set( assert_callback call );
 RGB_API RGB_NOTHROW
  assert_callback assert_get( );
}
