﻿//
// libRGB
// (c) 2015 Andrzej Budzanowski
//

#include "./libRGB.hpp"

//!
//! \defgroup group_allocators Alokatory
//! \brief
//!  W tej grupie znajdują się wszystkie alokatory i funkcje pomocnicze
//!  alokatorów które znajdują się w tej bibliotece.
//!

#pragma once

#include "./allocators_interface.hpp"
#include "./allocators_heap.hpp"
#include "./allocators_helpers.hpp"
#include "./allocators_pimp.hpp"
