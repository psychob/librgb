﻿//
// libRGB
// (c) 2015 Andrzej Budzanowski
//

#pragma once

//!
//! \file rgb/internal/library-options.hpp
//! \brief Plik w którym zdefiniowane są wszystkie opcje którymi można
//!  modyfikować działanie biblioteki.
//! \author Andrzej Budzanowski <psychob.pl@gmail.com>
//!
//! \warning Ten plik jest częścią wewnętrznego nie-publicznego API biblioteki,
//!  bezpośrednie dołączanie go do pliku poprzez dyrektywe `include` jest złym
//!  pomysłem. Jeśli chcesz dołączyć funkcjonalność tego pliku do swojego
//!  projektu to dołącz plik: rgb/libRGB.hpp.
//!
