﻿//
// libRGB
// (c) 2015 Andrzej Budzanowski
//

#pragma once

//!
//! \file rgb/internal/define-all-macros.hpp
//! \brief Plik definiujący wszystkie makra zależne od kompilatora.
//! \author Andrzej Budzanowski <psychob.pl@gmail.com>
//!
//! \warning Ten plik jest częścią wewnętrznego nie-publicznego API biblioteki,
//!  bezpośrednie dołączanie go do pliku poprzez dyrektywe `include` jest złym
//!  pomysłem. Jeśli chcesz dołączyć funkcjonalność tego pliku do swojego
//!  projektu to dołącz plik: rgb/libRGB.hpp.
//!

//!
//! \page page_using_adnotations Używanie adnotacji
//! \brief
//!  Kiedy należy - a kiedy nie - używać adnotacji które dostarcza biblioteka
//!  libRGB
//! \author Andrzej Budzanowski <psychob.pl@gmail.com>
//!
//! \link group_macro_adnotations Lista wszystkich adnotacji jest dostępna
//!  tutaj. \endlink
//!

//!
//! \defgroup group_macro_adnotations Lista Adnotacji
//! \brief
//!  W tej grupie znajdują się wszystkie adnotacje oferowane przez bibliotekę.
//!  Jeśli zastanawiasz się nad użyciem którejś z niżej opisanych najpierw
//!  przeczytaj \link page_using_adnotations Używanie adnotacji \endlink
//!

#ifdef RGB_BUILD
# define RGB_API       RGB_INTERNAL_COMPILER_PUBLIC_EXPORT
# define RGB_API_LOCAL RGB_INTERNAL_COMPILER_PRIVATE_EXPORT
#else
//!
//! \brief Makrodefinicja która informuje że dana funkcja należy do API
//!  biblioteki.
//!
# define RGB_API       RGB_INTERNAL_COMPILER_PUBLIC_IMPORT
//!
//! \brief Makrodefinicja która informuje że dana funkcja należy do API
//!  wewnętrznego biblioteki. Czyli takiego które jest dostępne dla wszystkich
//!  modułów biblioteki, ale nie dla aplikacji klienckich.
//!
# define RGB_API_LOCAL RGB_INTERNAL_COMPILER_PRIVATE_IMPORT
#endif // RGB_BUILD

#ifdef RGB_INTERNAL_COMPILER_NOTHROW
# define RGB_NOTHROW RGB_INTERNAL_COMPILER_NOTHROW
#else
//!
//! \brief
//!  Adontacja informująca kompilator że dana funkcja nie rzuci żadnego
//!  wyjątku.
//!
//! Przy napodkaniu funkcji oznaczonej tą adnotacja kompilator może założyć że
//!  dana funkcja **nigdy** nie rzuci żadnym wyjątkiem. Dzięki temu kompilator
//!  będzie mógł nie emitować kodu służącego za obsługę wyjątków.
//!
//! Użycie tej adnotacji różni się od korzystania z operatora throws w
//!  definicji funkcji tym, że przy roziązaniu throws kompilator musi się
//!  upewnić że z danej funkcji nie zostało rzucone nic innego i kompilator
//!  musi wygenerować dodatkowo trochę kodu (sprawdzenie czy nie został rzucony
//!  wyjątek + ubicie programu jeśli został rzucony zły wyjątek). Ta adnotacja
//!  zaś informuje kompilator że ta funkcja nie rzuci nic.
//!
//! \warning Jeśli spróbujesz rzucić wyjątek z funkcji oznaczonej jako
//!  RGB_NOTHROW, twój komputer eksploduje.
//!
//! \sa \link page_using_adnotations Używanie adnotacji \endlink
//!
//! \ingroup group_macro_adnotations
//!
# define RGB_NOTHROW
#endif // RGB_INTERNAL_COMPILER_NOTHROW

#ifdef RGB_INTERNAL_COMPILER_ALWAYS_INLINE
# define RGB_ALWAYS_INLINE RGB_INTERNAL_COMPILER_ALWAYS_INLINE
#else
//!
//! \brief
//!  Adnotacja informująca kompilator że dana funkcja musi być zinline'owana.
//!
//! \em Inlineing jest to technika kompilatora która pozwala kod funkcji która
//!  powinna być wywołana (na przykład poprzez mnemonik \em CALL), na
//!  wyciągnięcie go z danej funkcji i wsadzenie zamiast jej wywołania. Ta
//!  technika jest podstawą optymalizacji kompilatora dlatego że pozwala na
//!  zastosowanie innych technik optymalizacji. Czasami kompilator nie chce by
//!  dana funkcja była zinline'owana i trzeba go do tego zmusić. Do tego służy
//!  to makro.
//!
//! \warning Złe użycie tego makra może spowodować niepotrzebny rozrost kodu.
//! \remarks
//!  Kompilator może w pewnych sytuacjach zignorować tą adnotacje i nie
//!  zinline'ować kodu.
//! \remarks
//!  To makro nie działa jeśli kompilator i linker nie mają dostępu do kodu
//!  który ma być zinline'owany, na przykład jeśli korzystamy z biblioteki DLL
//!  albo linuxowego SharedObject.
//!
//! \sa \link page_using_adnotations Używanie adnotacji \endlink
//! \sa RGB_NEVER_INLINE
//!
//! \ingroup group_macro_adnotations
//!
# define RGB_ALWAYS_INLINE inline
#endif // RGB_INTERNAL_COMPILER_ALWAYS_INLINE

#ifdef RGB_INTERNAL_COMPILER_NEVER_INLINE
# define RGB_NEVER_INLINE RGB_INTERNAL_COMPILER_NEVER_INLINE
#else
//!
//! \brief
//!  Adnotacja informująca kompilator że dana funkcja **nie ma być**
//!  zinline'owana.
//!
//! \em Inlineing jest to technika kompilatora która pozwala kod funkcji która
//!  powinna być wywołana (na przykład poprzez mnemonik \em CALL), na
//!  wyciągnięcie go z danej funkcji i wsadzenie zamiast jej wywołania. Ta
//!  technika jest podstawą optymalizacji kompilatora dlatego że pozwala na
//!  zastosowanie innych technik optymalizacji. Czasami kompilator zbyt ochoczo
//!  chce inline'ować pewne funkcje. To makro służy by powiedzieć mu że tego
//!  nie chcemy.
//!
//! \warning Złe użycie tej adnotacji może spowodować że kod będzie się
//!  wykonywać wolniej.
//! \remarks
//!  Kompilator w pewnych sytuacjach może zignorować tą adnotacji i
//!  zinline'ować kod.
//!
//! \sa \link page_using_adnotations Używanie adnotacji \endlink
//! \sa RGB_ALWAYS_INLINE
//!
//! \ingroup group_macro_adnotations
//!
# define RGB_NEVER_INLINE
#endif // RGB_INTERNAL_COMPILER_NEVER_INLINE

#ifdef RGB_INTERNAL_COMPILER_PURE
# define RGB_PURE RGB_INTERNAL_COMPILER_PURE
#else
//!
//! \brief
//!  Adnotacja informująca że dana funkcja nie ma stanu, jej wynik zależy
//!  **tylko i wyłącznie** od jej parametrów. I jeśli ta funkcja przegląda
//!  globalną pamięć to tylko przez wskaźnik na stały obiekt przekazany w
//!  jej parametrze.
//!
//! \remarks
//!  Kompilator jeśli ma dostęp do kodu funkcji oznaczonej tą adnotacją może ją
//!  zignorować jeśli uzna że jest błędna. By temu się przeciwstawić należy
//!  przenieść definicje funkcji do innego modułu.
//! \warning Złe użycie tej adnotacji może prowadzić do trudnych do wykrycia
//!  błędów.
//!
//! \sa \link page_using_adnotations Używanie adnotacji \endlink
//! \sa RGB_CONST
//!
//! \ingroup group_macro_adnotations
//!
# define RGB_PURE
#endif // RGB_INTERNAL_COMPILER_PURE

#ifdef RGB_INTERNAL_COMPILER_CONST
# define RGB_CONST RGB_INTERNAL_COMPILER_CONST
#else
//!
//! \brief
//!  Adnotacja informująca że dana funkcja nie ma stanu, jej wynik zależy
//!  **tylko i wyłącznie** od jej parametrów. Funkcja nie może też przeglądać
//!  pamięci globalnej.
//!
//! \remarks
//!  Kompilator jeśli ma dostęp do kodu funkcji oznaczonej tą adnotacją może ją
//!  zignorować jeśli uzna że jest błędna. By temu się przeciwstawić należy
//!  przenieść definicje funkcji do innego modułu.
//! \warning Złe użycie tej adnotacji może prowadzić do trudnych do wykrycia
//!  błędów.
//!
//! \sa \link page_using_adnotations Używanie adnotacji \endlink
//! \sa RGB_PURE
//!
//! \ingroup group_macro_adnotations
//!
# define RGB_CONST
#endif // RGB_INTERNAL_COMPILER_PURE

#ifdef RGB_INTERNAL_COMPILER_COLD
# define RGB_COLD RGB_INTERNAL_COMPILER_COLD
#else
//!
//! \brief
//!  Adnotacja informująca że dana funkcja będzie rzadko używana.
//!
//! Ta adnotacja informuje kompilator że dana funkcja nie będzie często
//!  używana i może przenieść jej wywołania do innej sekcji kodu, na przykład
//!  do takiej w której procesor który będzie wykonywał program nie pobierze do
//!  icache (Instruction Cache) niepotrzebnych instrukcji których i tak nie
//!  wykona.
//!
//! Funkcje oznaczone tą adnotacją nie będą też \em inline'owane. Jeśli do
//!  wywołania tej funkcji będzie prowadziło jakieś wyrażenie warunkowe
//!  (\em if), to kompilator założy że dane wyrażenie jest mało prawdopodobne
//!  by zwróciło prawdę i może przenieść cały \em branch do innego miejsca.
//!
//! Niektóre systemy mają specjalne miejsce dla funkcji tego typu, co powoduje
//!  że będą one "razem" w pamięci co może poprawic \em code \em locality.
//!
//! \sa \link page_using_adnotations Używanie adnotacji \endlink
//! \sa RGB_HOT
//!
//! \ingroup group_macro_adnotations
//!
# define RGB_COLD RGB_NEVER_INLINE
#endif // RGB_INTERNAL_COMPILER_COLD


#ifdef RGB_INTERNAL_COMPILER_HOT
# define RGB_HOT RGB_INTERNAL_COMPILER_HOT
#else
//!
//! \brief
//!  Adnotacja informująca że dana funkcja będzie często używana.
//!
//! Ta adnotacja informuje kompilator że dana funkcja będzie często używana,
//!  dlatego kompilator będzie starał się wyoptymalizować daną funkcje tak
//!  by działała szybciej (kosztem wielkości kodu).
//!
//! Funkcje oznaczone tą adnotacją mogą być częściej \em inline'owane.
//!
//! Niektóre systemy mają specjalne miejsce dla funkcji tego typu, co
//!  spowoduje że będą "razem" w pamięci co poprawi \em code \em locality.
//!
//! \sa \link page_using_adnotations Używanie adnotacji \endlink
//! \sa RGB_HOT
//!
//! \ingroup group_macro_adnotations
//!
# define RGB_HOT
#endif // RGB_INTERNAL_COMPILER_HOT
