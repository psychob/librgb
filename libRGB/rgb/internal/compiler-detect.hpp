﻿//
// libRGB
// (c) 2015 Andrzej Budzanowski
//

#pragma once

//!
//! \file rgb/internal/compiler-detect.hpp
//! \brief Plik definiujący wszystkie makra kompilatorów, i dołączający
//!  odpowiednią
//! \author Andrzej Budzanowski <psychob.pl@gmail.com>
//!
//! \warning Ten plik jest częścią wewnętrznego nie-publicznego API biblioteki,
//!  bezpośrednie dołączanie go do pliku poprzez dyrektywe `include` jest złym
//!  pomysłem. Jeśli chcesz dołączyć funkcjonalność tego pliku do swojego
//!  projektu to dołącz plik: rgb/libRGB.hpp.
//!

// pierwsze co jest definiowane w tym pliku to wszystkie obsługiwane
// kompilatory w formie:
// RGB_COMPILER_NAZWA

// nasz pierwszy kompilator jest specjalny
#define RGB_COMPILER_UNKNONW 0

// Microsoft Visual C++
#define RGB_COMPILER_MSVCPP  1

// Gnu Compiler Collection
#define RGB_COMPILER_GCC     2

//! \file
//! \todo To powinno iść do pliku rgb/internal/compiler/msvcpp.hpp, aktualnie
//!  znajduje się to tutaj dlatego że biblioteka nie wspiera więcej
//!  kompilatorów niż msvcpp.

#define RGB_COMPILER RGB_COMPILER_MSVCPP

#define RGB_INTERNAL_COMPILER_PUBLIC_EXPORT __declspec(dllexport)
#define RGB_INTERNAL_COMPILER_PUBLIC_IMPORT __declspec(dllimport)

#define RGB_INTERNAL_COMPILER_PRIVATE_EXPORT
#define RGB_INTERNAL_COMPILER_PRIVATE_IMPORT
