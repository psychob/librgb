﻿//
// libRGB
// (c) 2015 Andrzej Budzanowski
//

#pragma once

//!
//! \file rgb/internal/macro-func.hpp
//! \brief Plik definiujący wszystkie podstawowe makrofunkcje z których
//!  korzysta biblioteka.
//! \author Andrzej Budzanowski <psychob.pl@gmail.com>
//!
//! \warning Ten plik jest częścią wewnętrznego nie-publicznego API biblioteki,
//!  bezpośrednie dołączanie go do pliku poprzez dyrektywe `include` jest złym
//!  pomysłem. Jeśli chcesz dołączyć funkcjonalność tego pliku do swojego
//!  projektu to dołącz plik: rgb/libRGB.hpp.
//!

//!
//! \internal
//! \brief Wewnętrzna makrofunkcja która scala dwa identyfikatory tworząc jeden
//! \param [in] x,y Identyfikatory które mają być scalone.
//!
//! Można spytać się dlaczego makrofunkcja #RGB_CAT wywołuje inną makrofunkcję,
//!  a nie scala identyfikatorów "od razu". Powodem tego jest pre-procesor c++.
//!  Jako że wszystkie identyfikatory są "rozszerzane" leniwie - czyli
//!  preprocesor nie rozszerzy jakiejś makrodefinicji jeśli nie będzie musiał.
//!  Jedna z możliwości jak wymusić na pre-procesorze rozszerzenie
//!  identyfikatorów jest poprzez wywołanie makrofunkcji.
//!
//! Przykład może to bardziej rozjaśni:
//! ~~~~{.cpp}
//!  #define foo baz
//!
//!  RGB_INTERNAL_CAT(foo, bar) // -> foobar
//!  RGB_CAT(foo,bar)           // -> bazbar
//! ~~~~
//!
#define RGB_INTERNAL_CAT(x,y) x ## y

//!
//! \brief Makrodefinicja pozwalająca na scalenie dwóch identyfikatorów.
//! \param [in] x,y Identyfikatory które mają być scalone
//!
#define RGB_CAT(x,y) RGB_INTERNAL_CAT(x,y)
