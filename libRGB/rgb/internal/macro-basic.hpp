﻿//
// libRGB
// (c) 2015 Andrzej Budzanowski
//

#pragma once

//!
//! \file rgb/internal/macro-basic.hpp
//! \brief Plik definiujący wszystkie podstawowe makra z których korzysta
//!  biblioteka.
//! \author Andrzej Budzanowski <psychob.pl@gmail.com>
//!
//! \warning Ten plik jest częścią wewnętrznego nie-publicznego API biblioteki,
//!  bezpośrednie dołączanie go do pliku poprzez dyrektywe `include` jest złym
//!  pomysłem. Jeśli chcesz dołączyć funkcjonalność tego pliku do swojego
//!  projektu to dołącz plik: rgb/libRGB.hpp.
//!

//!
//! \brief Makro odpowiadające wartości prawdziwej
//!
#define RGB_TRUE  1

//!
//! \brief Makro odpowiadające wartości fałszywej
//!
#define RGB_FALSE 0

//!
//! \brief Makro które rozszerza się do lini która jest aktualnie kompilowana.
//!
#define RGB_LINE __LINE__

//!
//! \brief Makro które rozszerze się do nazwy pliku który jest aktualnie
//!  kompilowany.
//!
#define RGB_FILE __FILE__
