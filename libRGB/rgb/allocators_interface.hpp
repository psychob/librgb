﻿//
// libRGB
// (c) 2015 Andrzej Budzanowski
//

#include "./libRGB.hpp"
#include "./types.hpp"

#pragma once

namespace rgb
{
 //!
 //! \brief
 //!  Interfejs alokatora.
 //! \author Andrzej Budzanowski <psychob.pl@gmail.com>
 //! \ingroup group_allocators
 //!
 class RGB_API allocator
 {
  public:
   //! \brief
   //!  Abstrakcja na zwracany przez alokator blok pamięci.
   using memory_block = void *;

   allocator( ) = default;
   virtual ~allocator( ) = default;

   allocator( const allocator & ) = default;
   allocator& operator=( const allocator & ) = default;

   allocator( allocator && dying );
   allocator& operator=( allocator && dying );

   //!
   //! \brief
   //!  Metoda która służy do alokowania \em count bajtów danych z systemu
   //!  operacyjnego, lub innego miejsca z którego będziemy brać pamięć.
   //! \param [in] count Ile bajtów alokator ma zaalokować.
   //! \retval nullptr Jeśli nie udało się zaalokować miejsca zostanie zwrócony
   //!  pusty wskaźnik.
   //! \return Jeśli alokacja się powiedzie, to zostanie zwrócony wskaźnik na
   //!  to miejsce.
   //! \remarks
   //!  Należy pamiętać że to od alokatora zależy ile miejsca w pamięci
   //!  zostanie zaalokowane. Zostanie zaalokowane zawsze **co najmniej** \em
   //!  bajtów.
   //! \remarks
   //!  Nie należy zakładać że alokator w jakiś sposób wyrówna nam adres
   //!  pamięci, chyba że jest to częścią oferowanego przez niego kontraktu
   //!  (sprawdź dokumentacje klasy).
   //!
   virtual memory_block allocate( nuint count ) = 0;
   //!
   //! \brief
   //!  Metoda która służy do dealokowanie określonego miejsca w pamięci.
   //! \param [in] m Adres miejsca które zostanie uwolnione do systemu
   //! \param [in] count Ile bajtów ma zostać uwolnione do systemu.
   //! \warning To od alokatora zależy czy wyczyści on cały blok pamięci, czy
   //!  tylko \em count pierwszych bajtów!
   virtual void deallocate( memory_block m, nuint count ) = 0;
 };
}
